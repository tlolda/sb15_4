#include <iostream>

// limit - �� ������ ����� ���������
// mode - ����� (true - ������ ��� false - ��������)
void findEverOrOdd(int limit, bool mode)
{
    if (mode == true)
    {
        for (int i = 0; i <= limit; i++)
        {
            if (i % 2 == 0)
                std::cout << i << "\n";
        }
    }
    else
    {
        for (int i = 0; i < limit; i++)
        {
            if (i % 2 != 0)
                std::cout << i << "\n";
        }
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");

    const int nCount = 37;
    bool mode = false; // true - ������, false - ��������

    // 1 - � ������� ����������� ����� (����, � ������� ��������� ������� main), ��������� ���� � �������, 
    // �������� � ������� ��� ������ ����� �� 0 �� N (N ������� ���������� � ������ ���������).
    std::cout << "������" << "\n";
    for (int i = 0; i <= nCount; i++)
    {
        if (i % 2 == 0)
            std::cout << i << "\n";
    }

    // 2 - �������� �������, ������� � ����������� �� ����� ���������� �������� � ������� ��� ������, ��� 
    // �������� ����� �� 0 �� N (N ���� �������� ���������� �������).
    /*!!!   ����� ����� � ���� �������� �������� � ��������� ���������� - ���������� �� ��������� � ���������� !!!
      !!!   �������� �������� �� ���������� "even" � "odd"                                                     !!!*/
    std::cout << "������, ����� - " << (mode ? "even" : "odd") << "\n"; 
    findEverOrOdd(nCount, mode);
    /* ��� �� 3 - ������������� ���������� ������ � �������.
       �� � ���� ��� �� ���� ��� �������������� � ������������� �� �. */ 
}